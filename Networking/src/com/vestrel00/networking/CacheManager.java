package com.vestrel00.networking;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class CacheManager {

	private static CacheManager sInstance;

	private LruCache<String, Bitmap> mBitmaps = new LruCache<String, Bitmap>(
			26 * 1024 * 1024) {
		protected int sizeOf(String key, Bitmap bitmap) {
			return bitmap.getRowBytes() * bitmap.getHeight();

		}
	};

	public static final CacheManager getInstance() {
		if (sInstance == null)
			sInstance = new CacheManager();

		return sInstance;
	}

	private CacheManager() {
	}

	public void addBitmap(String url, Bitmap bitmap) {
		mBitmaps.put(url, bitmap);
	}

	public Bitmap getBitmap(String url) {
		return mBitmaps.get(url);
	}

}
