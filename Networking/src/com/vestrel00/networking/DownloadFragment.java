package com.vestrel00.networking;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;

import com.vestrel00.networking.util.BitmapDownloader;

public class DownloadFragment extends Fragment {

	public static DownloadFragment newInstance() {
		DownloadFragment newFrag = new DownloadFragment();
		newFrag.setRetainInstance(true);
		return newFrag;
	}

	private EditText mETDownload;
	private Button mBDownload;
	private ViewSwitcher mVSImage;
	private LinearLayout mLLImages;

	private ArrayList<String> mBitmapUrls = new ArrayList<String>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_download, container,
				false);

		mETDownload = (EditText) view.findViewById(R.id.frag_download_et_url);
		mBDownload = (Button) view.findViewById(R.id.frag_download_b_start);
		mVSImage = (ViewSwitcher) view.findViewById(R.id.frag_download_vs_img);
		mLLImages = (LinearLayout) view.findViewById(R.id.frag_download_ll_img);

		mETDownload.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				mBDownload.setEnabled(URLUtil.isValidUrl(s.toString()));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

		});

		mBDownload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				downloadInBackground();
			}
		});

		CacheManager cache = CacheManager.getInstance();
		for (int i = 0; i < mBitmapUrls.size(); i++)
			addBitmap(cache.getBitmap(mBitmapUrls.get(i)));

		return view;
	}

	private void addBitmap(Bitmap bitmap) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParams.topMargin = getResources().getDimensionPixelSize(
				R.dimen.activity_vertical_margin);

		ImageView imageView = new ImageView(getActivity());
		imageView.setScaleType(ScaleType.FIT_CENTER);
		imageView.setLayoutParams(layoutParams);

		if (bitmap == null)
			imageView.setImageResource(R.drawable.ic_launcher);
		else
			imageView.setImageBitmap(bitmap);

		mLLImages.addView(imageView, 0);
	}

	private void downloadInBackground() {
		final String url = mETDownload.getText().toString();
		if (mBitmapUrls.indexOf(url) == -1)
			new MBitmapDownloader().execute(url);

	}

	private class MBitmapDownloader extends BitmapDownloader {

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			mBDownload.setEnabled(true);
			mETDownload.setEnabled(true);
			mVSImage.setDisplayedChild(0);

			final String url = mETDownload.getText().toString();
			CacheManager cacheManager = CacheManager.getInstance();

			if (cacheManager.getBitmap(url) != null)
				return;

			if (result != null)
				cacheManager.addBitmap(url, result);

			mBitmapUrls.add(url);

			addBitmap(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mBDownload.setEnabled(false);
			mETDownload.setEnabled(false);
			mVSImage.setDisplayedChild(1);

		}

	}

}
