package com.vestrel00.networking.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityUtil {

	public static boolean isConnected(Context context) {
		NetworkInfo netInfo = ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();

		return netInfo != null && netInfo.isConnected();
	}

}
