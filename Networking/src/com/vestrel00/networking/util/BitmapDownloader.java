package com.vestrel00.networking.util;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public abstract class BitmapDownloader extends Downloader<Bitmap> {

	@Override
	protected Bitmap getDataFromInputStream(InputStream is) {
		return BitmapFactory.decodeStream(is);
	}

}
