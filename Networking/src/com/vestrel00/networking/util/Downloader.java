package com.vestrel00.networking.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

public abstract class Downloader<T> extends AsyncTask<String, Integer, T> {

	@Override
	protected T doInBackground(String... urls) {

		try {
			return downloadUrl(urls[0]);

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	private T downloadUrl(String url) throws IOException {
		InputStream is = null;

		HttpURLConnection conn = (HttpURLConnection) new URL(url)
				.openConnection();
		conn.setReadTimeout(5000);
		conn.setConnectTimeout(5000);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);

		try {
			conn.connect();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new IOException(url + " failed");
			}

			is = conn.getInputStream();

			return getDataFromInputStream(is);

		} finally {
			if (is != null)
				is.close();
		}

	}

	abstract protected T getDataFromInputStream(InputStream is);

}
